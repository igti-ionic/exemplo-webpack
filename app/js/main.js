/**
 * Calculando linhas da tabela
 */
function calcularLinhasTabela() {
  /**
   * Obtendo referência da tabela pelo id
   */
  var tabela = document.querySelector("#tabela");

  /**
   * Obtendo referência do span pelo id
   */
  var span = document.querySelector("#numeroLinhas");

  /**
   * Efetuando o cálculo de linhas. O resultado do
   * cálculo é armazenado no conteúdo do span
   */
  span.textContent = tabela.children[2].children.length;
}

/**
 * Criando variável para armazenar referência
 * ao botão
 */
var botaoCalcular = document.querySelector("#botaoCalcular");

/**
 * Adicionando evento de clique ao botão
 *
 */
botaoCalcular.addEventListener("click", calcularLinhasTabela);
